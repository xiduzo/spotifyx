<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
*** https://github.com/othneildrew/Best-README-Template/blob/master/BLANK_README.md
-->

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

[![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![GNU GPLv3 License][license-shield]][https://gitlab.com/xiduzo/spotifyx/-/blob/main/license.txt]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/xiduzo/spotifyx">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">Fissa</h3>

  <p align="center">
    A collaborative spotify playlist
    <br />
    <a href="#docs"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://spotify-x.herokuapp.com/">View Demo</a>
    ·
    <a href="https://gitlab.com/xiduzo/spotifyx/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/xiduzo/spotifyx/-/merge_requests">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

[![Product Name Screen Shot][product-screenshot]](https://example.com)

This project is a exploration for a collaborative spotify playlist on parties. Users can decide the order of songs being played as a collective.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

- [React](https://reactjs.org/)
- [Mui](https://mui.com/)
- [Socket.io](https://socket.io/)
- [Express](https://expressjs.com/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Using docker

1. run `docker-compose build`
1. run `docker-compose up`

### Prerequisites

- For windows install `https://docs.docker.com/desktop/windows/install/`
  This is an example of how to list things you need to use the software and how to install them.

- npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. In the `frontend` and `websockets` folders run `npm i`
1. Run `npm run start` in both folders to start the project locally

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->

<!-- ROADMAP -->

## Roadmap

- [ ] Up and down voting
- [ ] Skipping artist
- [ ] Skipping genre
- [ ] Point system

See the [open issues](https://gitlab.com/xiduzo/spotifyx/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the GNU General Public License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

[@xiduzo](https://twitter.com/xiduzo) - mail@sanderboer.nl@mail@sanderboer.nl.com

Project Link: [https://gitlab.com/xiduzo/spotifyx](https://gitlab.com/xiduzo/spotifyx)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- ACKNOWLEDGMENTS -->

## Acknowledgments

- []()
- []()
- []()

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/gitlab/contributors/xiduzo/spotifyx.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/xiduzo/spotifyx/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/xiduzo/spotifyx.svg?style=for-the-badge
[forks-url]: https://gitlab.com/xiduzo/spotifyx/network/members
[stars-shield]: https://img.shields.io/gitlab/stars/xiduzo/spotifyx.svg?style=for-the-badge
[stars-url]: https://gitlab.com/xiduzo/spotifyx/stargazers
[issues-shield]: https://img.shields.io/gitlab/issues/xiduzo/spotifyx.svg?style=for-the-badge
[issues-url]: https://gitlab.com/xiduzo/spotifyx/issues
[license-shield]: https://img.shields.io/gitlab/license/xiduzo/spotifyx.svg?style=for-the-badge
[license-url]: https://gitlab.com/xiduzo/spotifyx/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/sander-boer-653110a5
[product-screenshot]: images/screenshot.png
